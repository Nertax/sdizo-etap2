#include<iostream>
#include<memory>
#include<list>
#include<climits>
#include<fstream>
#include<random>
#include<queue>
#include<vector>
#include"Graph.h"



#ifndef GraphList_h
#define GraphList_h


class GraphList {

    private:
        uint _edgeAmount, _vertexAmount, _firstVertex, _lastVertex;
        std::unique_ptr<std::list<VertexWithWeight>[]> _tab;

    public:
        GraphList();
        ~GraphList(); 

        int ReadDirectedGraphToMemory();
        int ReadUndirectedGraphToMemory();
        void Print();
        void Delete();
        std::list<Edge> MSTKruskal();
        std::list<Edge> MSTPrima();
        void PrintMST(std::list<Edge> finalEdgeList);
        void MakeUndirecteGraphFromFile(std::string fileName);
        void MakeDirecteGraphFromFile(std::string fileName);
        void printSPP(std::vector<DistanceWithPrevious> result);
        int MakeRandomGraph(int vertexAmount, int edgeAmount, bool directed, bool connected, bool nonnegativeOnly);
        std::vector<DistanceWithPrevious> SPPDijsktra();
        std::vector<DistanceWithPrevious> SPPDBellmanFord();
        void SetVertex();
};

#endif