#include"Measure.h"

void Measure::Start() {

    _PrintStart();
    _MeasureMenuLoop();
}



void Measure::_AutoMeasurements() {

    _PrintAutoMeasurements();

    int numberOfMeasurements = 100;

    std::queue<int> graphSizeList;

    graphSizeList.push(25);
    graphSizeList.push(50);
    graphSizeList.push(75);
    graphSizeList.push(100);
    graphSizeList.push(125);
    graphSizeList.push(150);
    graphSizeList.push(175);
    graphSizeList.push(200);
    graphSizeList.push(225);
    graphSizeList.push(250);
    graphSizeList.push(275);
    graphSizeList.push(300);


    while(!graphSizeList.empty()) {

        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.25, "m_mstp_100", GraphAlgorithms(MSTPrim)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.5, "m_mstp_100", GraphAlgorithms(MSTPrim)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.75, "m_mstp_100", GraphAlgorithms(MSTPrim)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.99, "m_mstp_100", GraphAlgorithms(MSTPrim)) != 1) break;

        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.25, "m_mstk_100", GraphAlgorithms(MSTKruskal)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.5, "m_mstk_100", GraphAlgorithms(MSTKruskal)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.75, "m_mstk_100", GraphAlgorithms(MSTKruskal)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.99, "m_mstk_100", GraphAlgorithms(MSTKruskal)) != 1) break;

        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.25, "m_sppd_100", GraphAlgorithms(SPPDijkstra)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.5, "m_sppd_100", GraphAlgorithms(SPPDijkstra)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.75, "m_sppd_100", GraphAlgorithms(SPPDijkstra)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.99, "m_sppd_100", GraphAlgorithms(SPPDijkstra)) != 1) break;

        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.25, "m_sppbf_100", GraphAlgorithms(SPPBellmanFord)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.5, "m_sppbf_100", GraphAlgorithms(SPPBellmanFord)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.75, "m_sppbf_100", GraphAlgorithms(SPPBellmanFord)) != 1) break;
        if(_ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSizeList.front(), 0.99, "m_sppbf_100", GraphAlgorithms(SPPBellmanFord)) != 1) break;

        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.25, "l_mstp_100", GraphAlgorithms(MSTPrim)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.5, "l_mstp_100", GraphAlgorithms(MSTPrim)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.75, "l_mstp_100", GraphAlgorithms(MSTPrim)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.99, "l_mstp_100", GraphAlgorithms(MSTPrim)) != 1) break;

        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.25, "l_mstk_100", GraphAlgorithms(MSTKruskal)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.5, "l_mstk_100", GraphAlgorithms(MSTKruskal)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.75, "l_mstk_100", GraphAlgorithms(MSTKruskal)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.99, "l_mstk_100", GraphAlgorithms(MSTKruskal)) != 1) break;

        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.25, "l_sppd_100", GraphAlgorithms(SPPDijkstra)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.5, "l_sppd_100", GraphAlgorithms(SPPDijkstra)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.75, "l_sppd_100", GraphAlgorithms(SPPDijkstra)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.99, "l_sppd_100", GraphAlgorithms(SPPDijkstra)) != 1) break;

        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.25, "l_sppbf_100", GraphAlgorithms(SPPBellmanFord)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.5, "l_sppbf_100", GraphAlgorithms(SPPBellmanFord)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.75, "l_sppbf_100", GraphAlgorithms(SPPBellmanFord)) != 1) break;
        if(_ExecuteMeasureGraphList(numberOfMeasurements, graphSizeList.front(), 0.99, "l_sppbf_100", GraphAlgorithms(SPPBellmanFord)) != 1) break;


        std::cout << "Dokonano pomiarow dla problemu o wielkosci " << graphSizeList.front() << std::endl;
        graphSizeList.pop();

    }

    std::cout << "Zakonczono automatyczne pomiary." << std::endl;
}




/* Metoda wywolujaca metode pomiarowa dla zadanego algorytmu grafu macierzowego (wskazana ilosc razy)
   i zapisujaca wyniki pomiaru do pilku o wskazanej nazwie

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie pomiarow, pomiar sie nie powiedzie lub czas pomairy wyjdzie ujemny (pomiary zostaja przerwane przy pierwszym blednym pomiarze)
   zwraca -1 Blad zapisu wyniku pomiaru do pliku
   zwraca -2 Nie udalo sie otworzyc pliku o podanej nazwie
*/
int Measure::_ExecuteMeasureGraphMatrix(int numberOfMeasurements, int graphSize, double graphDensity, std::string fileName, GraphAlgorithms algorithm) {

    std::fstream file;
    long timeMeasureResult;
    file.open(fileName.c_str(), std::ios::out | std::ios::app);

    if(file.is_open()) {

            file << graphSize << ";" << graphDensity << ";;";
            while(numberOfMeasurements--) {

                timeMeasureResult = _MeasureGraphMatrix(graphSize, graphDensity, algorithm);
                
                if(timeMeasureResult <= -1) {
                    std::cout << "Blad podczas pomiaru, przerywam pomiary" << std::endl;
                    return 0; 

                }

                file << timeMeasureResult << ';';

                if(file.fail()) 
                { 
                    std::cout << "Blad zapisu wyniku pomiaru do pliku, przerywam pomiary." << std::endl; 
                    return -1;
                } 
            }

            file << std::endl;
            file.close(); 

    } 
    else {

        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 
        return -2;
    }

    return 1;
    

}

/* Metoda wywolujaca metode pomiarowa dla zadanego algorytmu grafu listowego (wskazana ilosc razy)
   i zapisujaca wyniki pomiaru do pilku o wskazanej nazwie

   zwraca 1 jezeli operacja sie powiedzie
   zwraca 0 jezeli w trakcie pomiarow, pomiar sie nie powiedzie lub czas pomairy wyjdzie ujemny (pomiary zostaja przerwane przy pierwszym blednym pomiarze)
   zwraca -1 Blad zapisu wyniku pomiaru do pliku
   zwraca -2 Nie udalo sie otworzyc pliku o podanej nazwie
*/
int Measure::_ExecuteMeasureGraphList(int numberOfMeasurements, int graphSize, double graphDensity, std::string fileName, GraphAlgorithms algorithm) {

    std::fstream file;
    long timeMeasureResult;
    file.open(fileName.c_str(), std::ios::out | std::ios::app);

    if(file.is_open()) {

            file << graphSize << ";" << graphDensity << ";;";
            while(numberOfMeasurements--) {

                timeMeasureResult = _MeasureGraphList(graphSize, graphDensity, algorithm);
                
                if(timeMeasureResult <= -1) {
                    std::cout << "Blad podczas pomiaru, przerywam pomiary" << std::endl;
                    return 0; 

                }

                file << timeMeasureResult << ';';

                if(file.fail()) 
                { 
                    std::cout << "Blad zapisu wyniku pomiaru do pliku, przerywam pomiary." << std::endl; 
                    return -1;
                } 
            }

            file << std::endl;
            file.close(); 

    } 
    else {

        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 
        return -2;
    }

    return 1;
    

}




/* Metoda badajaca czas wykonania zadanego algorytmu grafu macierzowego
   na losowych danych o zadanej wielkosci i gestosci
*/
long Measure::_MeasureGraphMatrix(int graphSize, double graphDensity, GraphAlgorithms algorithm) {

    GraphMatrix graph;
    struct timespec a, b;

    if(graphSize <= 0 || graphDensity <= 0)
        return -1;
 

    if(algorithm == GraphAlgorithms(MSTPrim)) {

        uint maxEdgeAmount = graphSize * (graphSize - 1) / 2;

        if(graph.MakeRandomGraph(graphSize, (int)(graphDensity * maxEdgeAmount), 0, 1, 0) != 1) {

            std::cout << "Blad podczas tworzenia zadanej tablicy do pomiarow" << std::endl; 
            return -1;

        }

        clock_gettime(CLOCK_REALTIME, &a);
        graph.MSTPrima();
        clock_gettime(CLOCK_REALTIME, &b);
    }

    else if(algorithm == GraphAlgorithms(MSTKruskal)) {

        uint maxEdgeAmount = graphSize * (graphSize - 1) / 2;

        if(graph.MakeRandomGraph(graphSize, (int)(graphDensity * maxEdgeAmount), 0, 1, 0) != 1) {

            std::cout << "Blad podczas tworzenia zadanej tablicy do pomiarow" << std::endl; 
            return -1;

        }

        clock_gettime(CLOCK_REALTIME, &a);
        graph.MSTKruskal();
        clock_gettime(CLOCK_REALTIME, &b);
    }

    else if(algorithm == GraphAlgorithms(SPPDijkstra)) {

        uint maxEdgeAmount = graphSize * (graphSize - 1) / 2;

        if(graph.MakeRandomGraph(graphSize, (int)(graphDensity * maxEdgeAmount), 0, 1, 1) != 1) {

            std::cout << "Blad podczas tworzenia zadanej tablicy do pomiarow" << std::endl; 
            return -1;

        }

        clock_gettime(CLOCK_REALTIME, &a);
        graph.SPPDijsktra();
        clock_gettime(CLOCK_REALTIME, &b);
    }

    else if(algorithm == GraphAlgorithms(SPPBellmanFord)) {

        uint maxEdgeAmount = graphSize * (graphSize - 1) / 2;

        if(graph.MakeRandomGraph(graphSize, (int)(graphDensity * maxEdgeAmount), 0, 1, 1) != 1) {

            std::cout << "Blad podczas tworzenia zadanej tablicy do pomiarow" << std::endl; 
            return -1;

        }

        clock_gettime(CLOCK_REALTIME, &a);
        graph.SPPDBellmanFord();
        clock_gettime(CLOCK_REALTIME, &b);
    }
 
    
    graph.Delete();

    return _TimeDiff(a, b);

}



/* Metoda badajaca czas wykonania zadanego algorytmu w grafie listowym
   na losowych danych o zadanej wielkosci i gestosci
*/
long Measure::_MeasureGraphList(int graphSize, double graphDensity, GraphAlgorithms algorithm) {

    GraphList graph;
    struct timespec a, b;

    if(graphSize <= 0 || graphDensity <= 0)
        return -1;
 

    if(algorithm == GraphAlgorithms(MSTPrim)) {

        uint maxEdgeAmount = graphSize * (graphSize - 1) / 2;

        if(graph.MakeRandomGraph(graphSize, (int)(graphDensity * maxEdgeAmount), 0, 1, 0) != 1) {

            std::cout << "Blad podczas tworzenia zadanej tablicy do pomiarow" << std::endl; 
            return -1;

        }

        clock_gettime(CLOCK_REALTIME, &a);
        graph.MSTPrima();
        clock_gettime(CLOCK_REALTIME, &b);
    }

    else if(algorithm == GraphAlgorithms(MSTKruskal)) {

        uint maxEdgeAmount = graphSize * (graphSize - 1) / 2;

        if(graph.MakeRandomGraph(graphSize, (int)(graphDensity * maxEdgeAmount), 0, 1, 0) != 1) {

            std::cout << "Blad podczas tworzenia zadanej tablicy do pomiarow" << std::endl; 
            return -1;

        }

        clock_gettime(CLOCK_REALTIME, &a);
        graph.MSTKruskal();
        clock_gettime(CLOCK_REALTIME, &b);
    }

    else if(algorithm == GraphAlgorithms(SPPDijkstra)) {

        uint maxEdgeAmount = graphSize * (graphSize - 1) / 2;

        if(graph.MakeRandomGraph(graphSize, (int)(graphDensity * maxEdgeAmount), 0, 1, 1) != 1) {

            std::cout << "Blad podczas tworzenia zadanej tablicy do pomiarow" << std::endl; 
            return -1;

        }

        clock_gettime(CLOCK_REALTIME, &a);
        graph.SPPDijsktra();
        clock_gettime(CLOCK_REALTIME, &b);
    }

    else if(algorithm == GraphAlgorithms(SPPBellmanFord)) {

        uint maxEdgeAmount = graphSize * (graphSize - 1) / 2;

        if(graph.MakeRandomGraph(graphSize, (int)(graphDensity * maxEdgeAmount), 0, 1, 1) != 1) {

            std::cout << "Blad podczas tworzenia zadanej tablicy do pomiarow" << std::endl; 
            return -1;

        }

        clock_gettime(CLOCK_REALTIME, &a);
        graph.SPPDBellmanFord();
        clock_gettime(CLOCK_REALTIME, &b);
    }
 
    
    graph.Delete();

    return _TimeDiff(a, b);

}



void Measure::_MeasureGraphMatrixMenu() {


    while(1) {

        std::string action, fileName;
        int numberOfMeasurements, graphSize;
        double graphDensity;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintGraphMatrixHelp(); 

        else if(action == "mstp" || action == "mstprima") {
            std::cin >> numberOfMeasurements >> graphSize >> graphDensity >> fileName;
            _ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSize, graphDensity, fileName, GraphAlgorithms(MSTPrim));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        } 

        else if(action == "mstk" || action == "mstkruskal") {
            std::cin >> numberOfMeasurements >> graphSize >> graphDensity >> fileName;
            _ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSize, graphDensity, fileName, GraphAlgorithms(MSTKruskal));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        }

        else if(action == "sppd" || action == "sppdijkstra") {
            std::cin >> numberOfMeasurements >> graphSize >> graphDensity >> fileName;
            _ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSize, graphDensity, fileName, GraphAlgorithms(SPPDijkstra));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        }

        else if(action == "sppbf" || action == "sppbellmanford") {
            std::cin >> numberOfMeasurements >> graphSize >> graphDensity >> fileName;
            _ExecuteMeasureGraphMatrix(numberOfMeasurements, graphSize, graphDensity, fileName, GraphAlgorithms(SPPBellmanFord));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        }
 
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }
}


void Measure::_MeasureGraphListMenu() {

    while(1) {

        std::string action, fileName;
        int numberOfMeasurements, graphSize;
        double graphDensity;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintGraphListHelp(); 

        else if(action == "mstp" || action == "mstprima") {
            std::cin >> numberOfMeasurements >> graphSize >> graphDensity >> fileName;
            _ExecuteMeasureGraphList(numberOfMeasurements, graphSize, graphDensity, fileName, GraphAlgorithms(MSTPrim));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        } 

        else if(action == "mstk" || action == "mstkruskal") {
            std::cin >> numberOfMeasurements >> graphSize >> graphDensity >> fileName;
            _ExecuteMeasureGraphList(numberOfMeasurements, graphSize, graphDensity, fileName, GraphAlgorithms(MSTKruskal));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        }

        else if(action == "sppd" || action == "sppdijkstra") {
            std::cin >> numberOfMeasurements >> graphSize >> graphDensity >> fileName;
            _ExecuteMeasureGraphList(numberOfMeasurements, graphSize, graphDensity, fileName, GraphAlgorithms(SPPDijkstra));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        }

        else if(action == "sppbf" || action == "sppbellmanford") {
            std::cin >> numberOfMeasurements >> graphSize >> graphDensity >> fileName;
            _ExecuteMeasureGraphList(numberOfMeasurements, graphSize, graphDensity, fileName, GraphAlgorithms(SPPBellmanFord));
            std::cout << "Pomiar zakonczony." << std::endl;
           
        }
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }
}

//glowna petla measure menu
void Measure::_MeasureMenuLoop() {

    while(1) {

        std::string action;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintHelp();

        else if(action == "gm" || action == "graphmatrix")
            _MeasureGraphMatrixMenu();

        else if(action == "gl" || action == "graphlist")
            _MeasureGraphListMenu();

        else if(action == "auto")
            _AutoMeasurements();

        else if(action == "q" || action == "quit")
            return;

        else
            _PrintNoSuchAction();

    }


}


//napis przed automatycznymi pomiarami
void Measure::_PrintAutoMeasurements() {

    std::cout << "Zostaly uruchomione automatyczne pomiary. Czas zbierania danch pomiarowych moze wyniesc kilkanascie/kilkadziesiat minut. Dane zostana wygenerowane do plikow nazywanych w nastepujacy sposob - x_y_z, gdzie:" << std::endl
              << "x oznacza rodzaj trzymania grafu w pamieci - m - macierz sasiedztwa, l - listy sasiedztwa," << std::endl
              << "y oznacza mierzony algorytm, nazwy zgodne ze skrotami, przy recznych testach" << std::endl
              << "z oznacza ilosc powtorzen dla kazdego rozmiaru i gestosci grafu - do usrednienia wyniku" << std::endl
              << "kazdy pomiar zostanie wykonany dla nastepujacych wielkosci struktur: 25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300 oraz dla nastepujacych gestosci: 0.25, 0.5, 0.75, 0.99" << std::endl
              << "Czekaj na komunikat o zakonczeniu pomiarow" << std::endl;
}

//napis helpa z menu grafu listowego
void Measure::_PrintGraphListHelp() {


    std::cout << "Aktualnie wykonujesz pomiary na reprezentacji listowej, aby wykonac pomiar musisz podac piec argumentow oddzielonych bialym znakiem: " << std::endl
              << "pierwszy to rodzaj algorytmu:" << std::endl
              << "  SPP Dijkstra - wpisz sppd/sppdijkstra" << std::endl
              << "  SPP Bellman-Ford - wpisz sppbf/sppbellmanford" << std::endl
              << "  MST Kruskal - wpisz mstk/mstkruskal" << std::endl
              << "  MST Prim - wpisz mstp/mstprima" << std::endl
              << "drugi to ilosc powtorzen pomiaru" << std::endl
              << "trzeci to wielkosc grafu" << std::endl
              << "czwarty to gestosc grafu" << std::endl
              << "piaty to nazwa pliku do ktorego zapisane zostana wyniki pomiarow" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;

}


//napis helpa z menu grafu macierzowego
void Measure::_PrintGraphMatrixHelp() {


    std::cout << "Aktualnie wykonujesz pomiary na reprezentacji macierzowej, aby wykonac pomiar musisz podac piec argumentow oddzielonych bialym znakiem: " << std::endl
              << "pierwszy to rodzaj algorytmu:" << std::endl
              << "  SPP Dijkstra - wpisz sppd/sppdijkstra" << std::endl
              << "  SPP Bellman-Ford - wpisz sppbf/sppbellmanford" << std::endl
              << "  MST Kruskal - wpisz mstk/mstkruskal" << std::endl
              << "  MST Prim - wpisz mstp/mstprima" << std::endl
              << "drugi to ilosc powtorzen pomiaru" << std::endl
              << "trzeci to wielkosc grafu" << std::endl
              << "czwarty to gestosc grafu" << std::endl
              << "piaty to nazwa pliku do ktorego zapisane zostana wyniki pomiarow" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;

}


//napis helpa z glownego menu
void Measure::_PrintHelp() {

    std::cout << "Nalezy wybrac sposob trzymania grafu w pamieci: " << std::endl
              << "Listy sasiedztwa  - wpisz gl/graphlist" << std::endl
              << "Macierz sasiedztwa z wagami  - wpisz gm/graphmatrix" << std::endl
              << "Aby wykonac automatyczne pomiary - wpisz auto" << std::endl
              << "Aby wyjsc z programu wpisz q/quit" << std::endl << std::endl;
}

//napis ze nie ma takiej akcji
void Measure::_PrintNoSuchAction() {

    std::cout << "Nie znaleziono takiej akcji. Prosze wybrac ponownie." << std::endl
              << "Polecenie h/help wyswietla pomoc." << std::endl;

}


//napis startowy w glownym menu
void Measure::_PrintStart() {

    std::cout << "Program zostal uruchomiony w trybie pomiarow. " << std::endl
              << "Mozna w nim mierzyc czas wykonywania pooszczegolnych algorytmow, dla wybranej wielkosci problemu." << std::endl
              << "Aby wyswietlic pomoc i dowiedziec sie wiecej wpisz h/help" << std::endl << std::endl;
    
}

//metoda obliczajaca rzeczywisty czas w ns pomiedzy dwoma pomiarami aktualnego czasu
long Measure::_TimeDiff(timespec start, timespec end) {

    timespec temp;

    if ((end.tv_nsec - start.tv_nsec) < 0) {
		temp.tv_sec = end.tv_sec - start.tv_sec - 1;
		temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec - start.tv_sec;
		temp.tv_nsec = end.tv_nsec - start.tv_nsec;
	}

    return temp.tv_sec * 1000000000 + temp.tv_nsec;
}
