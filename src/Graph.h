#include<iostream>

#ifndef Graph_h
#define Graph_h



class Edge {

    public:
        uint FirstVertex, SecondVertex;
        int Weight;
        Edge(uint firstVertex, uint secondVertex, int weight) { FirstVertex = firstVertex; SecondVertex = secondVertex; Weight = weight; }
};


class VertexWithWeight {

    public:
        uint Vertex;
        int Weight;
        VertexWithWeight(uint vertex, int weight) { Vertex = vertex; Weight = weight; }
};


class DistanceWithPrevious {

    public:
        long Distance;
        int Previous;
        DistanceWithPrevious(long distance, int previous) { Distance = distance; Previous = previous; }
        DistanceWithPrevious() = default;

};

class VertexWithDistance {

    public:
        long Distance;
        int Vertex;
        VertexWithDistance(int vertex, long distance) { Vertex = vertex; Distance = distance; }

};

#endif