#include<iostream>
#include<fstream>
#include<time.h>
#include<random>
#include<algorithm>
#include<climits>
#include<queue>
#include"GraphList.h"
#include"GraphMatrix.h"


#ifndef Measure_h
#define Measure_h


enum GraphAlgorithms {

    MSTPrim,
    MSTKruskal,
    SPPDijkstra,
    SPPBellmanFord,
    MRG
};


class Measure {

    private:
        void _AutoMeasurements();
        int _ExecuteMeasureGraphMatrix(int numberOfMeasurements, int graphSize, double graphDensity, std::string fileName, GraphAlgorithms algorithm);
        int _ExecuteMeasureGraphList(int numberOfMeasurements, int graphSize, double graphDensity, std::string fileName, GraphAlgorithms algorithm);

        long _MeasureGraphMatrix(int graphSize, double graphDensity, GraphAlgorithms algorithm);
        long _MeasureGraphList(int graphSize, double graphDensity, GraphAlgorithms algorithm);

        void _MeasureGraphMatrixMenu();
        void _MeasureGraphListMenu();

        void _MeasureMenuLoop();
        
        void _PrintAutoMeasurements();
        void _PrintGraphListHelp();
        void _PrintGraphMatrixHelp();
        void _PrintHelp();
        void _PrintNoSuchAction();
        void _PrintStart();

        long _TimeDiff(timespec start, timespec end);
   
    public:

        void Start();
};

#endif