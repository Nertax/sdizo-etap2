#include"Menu.h"

//metoda startowa menu
void Menu::Start() {

    _PrintStart();
    _MenuLoop();
}


//glowna petla menu
void Menu::_MenuLoop() {

    while(1) {

        std::string action;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);


        if(action == "h" || action == "help")
            _PrintHelp();

        else if(action == "gm" || action == "graphmatrix")
            _SubMenuGraphMatrix();

        else if(action == "gl" || action == "graphlist")
            _SubMenuGraphList();

        else if(action == "q" || action == "quit")
            return;

        else
            _PrintNoSuchAction();

    }


}

//napis helpa z menu grafu listowego
void Menu::_PrintGraphListHelp() {


    std::cout << "Aktualnie pracujesz na reprezentacji listowej, mozliwe akcje to: " << std::endl
              << "Tworzenie losowego grafu - wpisz mrg/makerandomgraph i podaj liczbe wierzcholkow, liczbe krawedzi, czy graf ma byc skierowany, czy graf ma byc spojny, czy wagi grafu musza byc nieujemne" << std::endl
              << "Ustawianie wierzcholka poczatkowego i koncowegi - wpisz sv/setvertex" << std::endl
              << "SPP Dijkstra - wpisz sppd/sppdijkstra" << std::endl
              << "SPP Bellman-Ford - wpisz sppbf/sppbellmanford" << std::endl
              << "MST Kruskal - wpisz mstk/mstkruskal" << std::endl
              << "MST Prim - wpisz mstp/mstprima" << std::endl
              << "Wczytywanie z stdin (undirected) - wpisz ru/readundirected i podaj dane w takim samym formacie jak w pliku" << std::endl
              << "Wczytywanie z stdin (directed) - wpisz rd/readdirected i podaj dane w takim samym formacie jak w pliku" << std::endl
              << "Wczytywanie z pliku (undirected - zawsze podwaja) - wpisz mugff/makeundirectegraphfromfile i podaj nazwe pliku" << std::endl
              << "Wczytywanie z pliku (directed - wczytuje tak jak jest) - wpisz mdgff/makedirectegraphfromfile i podaj nazwe pliku" << std::endl
              << "Wypisywanie grafu - wpisz p/print" << std::endl
              << "Usuwanie grafu - wpisz d/delete" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;


}

//napis helpa z menu grafu macierzowego
void Menu::_PrintGraphMatrixHelp() {


    std::cout << "Aktualnie pracujesz na reprezentacji macierzowej, mozliwe akcje to: " << std::endl
              << "Tworzenie losowego grafu - wpisz mrg/makerandomgraph i podaj liczbe wierzcholkow, liczbe krawedzi, czy graf ma byc skierowany, czy graf ma byc spojny, czy wagi grafu musza byc nieujemne" << std::endl
              << "Ustawianie wierzcholka poczatkowego i koncowegi - wpisz sv/setvertex" << std::endl
              << "SPP Dijkstra - wpisz sppd/sppdijkstra" << std::endl
              << "SPP Bellman-Ford - wpisz sppbf/sppbellmanford" << std::endl
              << "MST Kruskal - wpisz mstk/mstkruskal" << std::endl
              << "MST Prim - wpisz mstp/mstprima" << std::endl
              << "Wczytywanie z stdin (undirected) - wpisz ru/readundirected i podaj dane w takim samym formacie jak w pliku" << std::endl
              << "Wczytywanie z stdin (directed) - wpisz rd/readdirected i podaj dane w takim samym formacie jak w pliku" << std::endl
              << "Wczytywanie z pliku (undirected - zawsze podwaja) - wpisz mugff/makeundirectegraphfromfile i podaj nazwe pliku" << std::endl
              << "Wczytywanie z pliku (directed - wczytuje tak jak jest) - wpisz mdgff/makedirectegraphfromfile i podaj nazwe pliku" << std::endl
              << "Wypisywanie grafu - wpisz p/print" << std::endl
              << "Usuwanie grafu - wpisz d/delete" << std::endl
              << "Aby wyjsc do menu glownego wpisz ret/return" << std::endl << std::endl;


}

//napis helpa z glownego menu
void Menu::_PrintHelp() {

    std::cout << "Nalezy wybrac sposob trzymania grafu w pamieci: " << std::endl
              << "Listy sasiedztwa  - wpisz gl/graphlist" << std::endl
              << "Macierz sasiedztwa z wagami  - wpisz gm/graphmatrix" << std::endl
              << "Aby wyjsc z programu wpisz q/quit" << std::endl << std::endl;
    
}

//napis ze nie ma takiej akcji
void Menu::_PrintNoSuchAction() {

    std::cout << "Nie znaleziono takiej akcji. Prosze wybrac ponownie." << std::endl
              << "Polecenie h/help wyswietla pomoc." << std::endl;

}

//napis startowy w glownym menu
void Menu::_PrintStart() {

    std::cout << "Program zostal uruchomiony w trybie recznego analizowania problemow. " << std::endl
              << "Mozna w nim miedzy innymi testowac poprawnosc dzialania algorytmow." << std::endl
              << "Aby wyswietlic pomoc w trakcie pracy wpisz h/help" << std::endl << std::endl;
    
}


//petla podmenu dla grafu reprezentowanego w postaci tablicy list
void Menu::_SubMenuGraphList() {

    while(1) {

        std::string action, fileName;
        int a, b;
        bool c, d, e;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintGraphListHelp();

        else if(action == "mrg" || action == "makerandomgraph") {
            std::cin >> a >> b >> c >> d >> e;
            _menuGraphList.MakeRandomGraph(a, b, c, d, e);
        }

        else if(action == "sv" || action == "setvertex") {
            _menuGraphList.SetVertex();
        }

        else if(action == "sppd" || action == "sppdijkstra") {
            _menuGraphList.printSPP(_menuGraphList.SPPDijsktra());
        }

        else if(action == "sppbf" || action == "sppbellmanford") {
            _menuGraphList.printSPP(_menuGraphList.SPPDBellmanFord());
        }

        else if(action == "mugff" || action == "makeundirectegraphfromfile") {
            std::cin >> fileName;
            _menuGraphList.MakeUndirecteGraphFromFile(fileName);
        }

        else if(action == "mdgff" || action == "makedirectegraphfromfile") {
            std::cin >> fileName;
            _menuGraphList.MakeDirecteGraphFromFile(fileName);
        }

        else if(action == "mstk" || action == "mstkruskal") {
            _menuGraphList.PrintMST(_menuGraphList.MSTKruskal());
        }

        else if(action == "mstp" || action == "mstprima") {
            _menuGraphList.PrintMST(_menuGraphList.MSTPrima());
        }

        else if(action == "p" || action == "print") {
            _menuGraphList.Print();
        }

        else if(action == "rd" || action == "readdirected") {
            _menuGraphList.ReadDirectedGraphToMemory();
        }

        else if(action == "ru" || action == "readundirected") {
            _menuGraphList.ReadUndirectedGraphToMemory();
        }

        else if(action == "d" || action == "delete") {
            _menuGraphList.Delete();
        } 
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }


}

//petla podmenu dla grafu reprezentowanego w postaci macierzy
void Menu::_SubMenuGraphMatrix() {

    while(1) {

        std::string action, fileName;
        int a, b;
        bool c, d, e;
        std::cin >> action;
        std::transform(action.begin(), action.end(), action.begin(), tolower);

        if(action == "h" || action == "help")
            _PrintGraphMatrixHelp();

        else if(action == "mrg" || action == "makerandomgraph") {
            std::cin >> a >> b >> c >> d >> e;
            _menuGraphMatrix.MakeRandomGraph(a, b, c, d, e);
        }

        else if(action == "sv" || action == "setvertex") {
            _menuGraphMatrix.SetVertex();
        }

        else if(action == "sppd" || action == "sppdijkstra") {
            _menuGraphMatrix.printSPP(_menuGraphMatrix.SPPDijsktra());
        }

        else if(action == "sppbf" || action == "sppbellmanford") {
            _menuGraphMatrix.printSPP(_menuGraphMatrix.SPPDBellmanFord());
        }

        else if(action == "mugff" || action == "makeundirectegraphfromfile") {
            std::cin >> fileName;
            _menuGraphMatrix.MakeUndirecteGraphFromFile(fileName);
        }

        else if(action == "mdgff" || action == "makedirectegraphfromfile") {
            std::cin >> fileName;
            _menuGraphMatrix.MakeDirecteGraphFromFile(fileName);
        }

        else if(action == "mstk" || action == "mstkruskal") {
            _menuGraphMatrix.PrintMST(_menuGraphMatrix.MSTKruskal());
        }

        else if(action == "mstp" || action == "mstprima") {
            _menuGraphMatrix.PrintMST(_menuGraphMatrix.MSTPrima());
        }

        else if(action == "p" || action == "print") {
            _menuGraphMatrix.Print();
        }

        else if(action == "rd" || action == "readdirected") {
            _menuGraphMatrix.ReadDirectedGraphToMemory();
        }

        else if(action == "ru" || action == "readundirected") {
            _menuGraphMatrix.ReadUndirectedGraphToMemory();
        }

        else if(action == "d" || action == "delete") {
            _menuGraphMatrix.Delete();
        } 
        
        else if(action == "ret" || action == "return") {
            return;
        }

        else
            _PrintNoSuchAction();

    }


}