#include"GraphMatrix.h"

GraphMatrix::GraphMatrix() {
    
    _edgeAmount = 0;
    _vertexAmount = 0;
    _firstVertex = 0;
    _lastVertex = 0;

}

GraphMatrix::~GraphMatrix() {

    Delete();    
}


void GraphMatrix::Delete() {

    _edgeAmount = 0;
    _vertexAmount = 0;
    _firstVertex = 0;
    _lastVertex = 0;

    
    for(uint i = 0; i < _vertexAmount; i++)
        _matrix[i].reset(nullptr);
 
    _matrix.reset(nullptr);
    
}

void GraphMatrix::SetVertex() {

    std::cout << "Prosze podac wierzcholek poczatkowy i koncowy: ";
    std::cin >>  _firstVertex >> _lastVertex;

}



/*
Wersja wczytujaca graf skierowany
moze wczytac graf nieskierowany zakładając, że użytkownik pod dana krawedz dwukrotnie, raz w jedna strone, a raz w druga,
przy czym podanie raz jednej wagi, a raz innej spowoduje niezdefiniowane zachowanie algorytmu 


zwraca 1 jesli wszystko dobrze
zwraca 0 jesli nie uda mu sie zaalokowac pamieci do reprezentacji grafu
*/
int GraphMatrix::ReadDirectedGraphToMemory() {

    uint v0, v1;
    int weight;
    std::cin >> _edgeAmount >> _vertexAmount >> _firstVertex >> _lastVertex;

    _matrix.reset(new std::unique_ptr<int[]>[_vertexAmount]);
    if(_matrix == nullptr) return 0;
    else {
        for(uint i = 0; i < _vertexAmount; i++) {

            _matrix[i].reset(new (std::nothrow) int[_vertexAmount]);
            if(_matrix[i] == nullptr) return 0;

            for(uint j = 0; j < _vertexAmount; j++) 
                _matrix[i][j] = INT_MAX;
        }
    }

    for(uint i = 0; i < _edgeAmount; i++) {

        std::cin >> v0 >> v1 >> weight;
        _matrix[v0][v1] = weight;
    }

    return 1;

}

/*
Wersja wczytujaca graf nieskierowany, przy zalozeniu, ze uzytkownik podaje dana krawedz tylko raz w jedna ze stron
Algorytm sam dopisze sobie w pamieci, ze dana krawedz idzie takze w druga strone z taka sama waga (krawedz nieskierowana)


zwraca 1 jesli wszystko dobrze
zwraca 0 jesli nie uda mu sie zaalokowac pamieci do reprezentacji grafu
*/
int GraphMatrix::ReadUndirectedGraphToMemory() {

    uint v0, v1;
    int weight;
    std::cin >> _edgeAmount >> _vertexAmount >> _firstVertex >> _lastVertex;

    _matrix.reset(new std::unique_ptr<int[]>[_vertexAmount]);
    if(_matrix == nullptr) return 0;
    else {
        for(uint i = 0; i < _vertexAmount; i++) {

            _matrix[i].reset(new (std::nothrow) int[_vertexAmount]);
            if(_matrix[i] == nullptr) return 0;

            for(uint j = 0; j < _vertexAmount; j++) 
                _matrix[i][j] = INT_MAX;
        }
    }

    for(uint i = 0; i < _edgeAmount; i++) {

        std::cin >> v0 >> v1 >> weight;
        _matrix[v0][v1] = weight;
        _matrix[v1][v0] = weight;
    }

    return 1;

}




void GraphMatrix::Print() {

    for(uint i = 0; i < _vertexAmount; i++) {
        for(uint j = 0; j < _vertexAmount; j++) 
            if(_matrix[i][j] != INT_MAX)
                std::cout << _matrix[i][j] << " ";
            else    
                std::cout << "- ";
        
        std::cout << std::endl;
    }

}


void GraphMatrix::PrintMST(std::list<Edge> finalEdgeList) {

    long totalWeight = 0;

    while(!finalEdgeList.empty()) {
        totalWeight += finalEdgeList.front().Weight;
        std::cout << finalEdgeList.front().FirstVertex << " " << finalEdgeList.front().SecondVertex << " " << finalEdgeList.front().Weight << std::endl;
        finalEdgeList.pop_front();

    }

    std::cout << "Waga MST: " << totalWeight << std::endl;

}

std::list<Edge> GraphMatrix::MSTKruskal() {

//w pamieci zakladm, ze graf jest trzymany jako nieskierowany

    std::list<Edge> initialEdgeList;
    std::list<Edge> finalEdgeList;

    for(uint i = 0; i < _vertexAmount; i++) 
        for(uint j = 0; j < _vertexAmount; j++) 
            if(_matrix[i][j] != INT_MAX ) 
                initialEdgeList.push_front(Edge(i, j, _matrix[i][j]));

    
    initialEdgeList.sort([](const Edge &a, const Edge &b) { return a.Weight < b.Weight; }); 

    uint tab[_vertexAmount];

    for(uint i = 0; i < _vertexAmount; i++) 
        tab[i] = i;


    while(  !(finalEdgeList.size() == _vertexAmount -1)  && !initialEdgeList.empty()  ) {

        if(tab[initialEdgeList.front().FirstVertex] != tab[initialEdgeList.front().SecondVertex]) {

            uint tempID = tab[initialEdgeList.front().SecondVertex];
            for(uint i = 0; i < _vertexAmount; i++) 
                if(tab[i] == tempID)
                    tab[i] = tab[initialEdgeList.front().FirstVertex];


            finalEdgeList.push_back(initialEdgeList.front());
            initialEdgeList.pop_front();


        }
        else {

            initialEdgeList.pop_front();
        }
    }

    

    return finalEdgeList;

}



std::list<Edge> GraphMatrix::MSTPrima() {

//w pamieci zakladm, ze graf jest trzymany jako nieskierowany


    auto comp = [](const Edge &a, const Edge &b) { return a.Weight > b.Weight; };
    std::priority_queue<Edge, std::vector<Edge>, decltype(comp)> tempEdgeList(comp);
    std::list<Edge> finalEdgeList;

    bool usedVertex[_vertexAmount];

    for(uint i = 0; i < _vertexAmount; i++) 
        usedVertex[i] = false;


    uint lastAddedVertex = _firstVertex; 

    usedVertex[lastAddedVertex] = true;

    while(!(finalEdgeList.size() == _vertexAmount -1)) {


        for(uint i = 0; i < _vertexAmount; i++)
            if(_matrix[lastAddedVertex][i] != INT_MAX)
                tempEdgeList.push(Edge(lastAddedVertex, i, _matrix[lastAddedVertex][i])); 
       

        while(1) {

            if(usedVertex[tempEdgeList.top().FirstVertex] == true && usedVertex[tempEdgeList.top().SecondVertex] == true) {

                tempEdgeList.pop();
            }
            else {

                finalEdgeList.push_back(tempEdgeList.top());

                if(usedVertex[tempEdgeList.top().FirstVertex] == false) {

                    lastAddedVertex = tempEdgeList.top().FirstVertex;
                    usedVertex[tempEdgeList.top().FirstVertex] = true;
                }
                else {

                    lastAddedVertex = tempEdgeList.top().SecondVertex;
                    usedVertex[tempEdgeList.top().SecondVertex] = true;
                }

                tempEdgeList.pop();
                break;
            }
        }
    }
  

    return finalEdgeList;

}


void GraphMatrix::MakeUndirecteGraphFromFile(std::string fileName) {

    Delete();

    uint v0, v1;
    int weight;

    std::ifstream file;
    file.open(fileName.c_str());

    if(file.is_open()) {
    
        file >> _edgeAmount >> _vertexAmount >> _firstVertex >> _lastVertex;
        if(file.fail()) 
            std::cout << "Nie udalo sie wczytac pierwszej linii danych ogolnych z pliku" << std::endl; 
        else {
            
            
            _matrix.reset(new std::unique_ptr<int[]>[_vertexAmount]);
            if(_matrix == nullptr)
                std::cout << "Nie udalo zaalkowac pamieci dla grafu" << std::endl;
            else {
                for(uint i = 0; i < _vertexAmount; i++) {

                    _matrix[i].reset(new (std::nothrow) int[_vertexAmount]);
                    if(_matrix[i] == nullptr)
                        std::cout << "Nie udalo zaalkowac pamieci dla grafu" << std::endl;

                    for(uint j = 0; j < _vertexAmount; j++) 
                        _matrix[i][j] = INT_MAX;
                }
            }

            for(uint i = 0; i < _edgeAmount; i++) {
            
                file >> v0 >> v1 >> weight;
                if(file.fail()) {
                
                    std::cout << "Blad odczytu danych, udalo sie wczytac: " << i << " danych poprawnie, przerywam dalsze wczytywanie" << std::endl; 
                    break; 
                } 
                else {

                    _matrix[v0][v1] = weight;
                    _matrix[v1][v0] = weight;
                }
            }
        }

            file.close(); 
    }
    else 
        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 

}


void GraphMatrix::MakeDirecteGraphFromFile(std::string fileName) {

    Delete();

    uint v0, v1;
    int weight;

    std::ifstream file;
    file.open(fileName.c_str());

    if(file.is_open()) {
    
        file >> _edgeAmount >> _vertexAmount >> _firstVertex >> _lastVertex;
        if(file.fail()) 
            std::cout << "Nie udalo sie wczytac pierwszej linii danych ogolnych z pliku" << std::endl; 
        else {
            
            
            _matrix.reset(new std::unique_ptr<int[]>[_vertexAmount]);
            if(_matrix == nullptr)
                std::cout << "Nie udalo zaalkowac pamieci dla grafu" << std::endl;
            else {
                for(uint i = 0; i < _vertexAmount; i++) {

                    _matrix[i].reset(new (std::nothrow) int[_vertexAmount]);
                    if(_matrix[i] == nullptr)
                        std::cout << "Nie udalo zaalkowac pamieci dla grafu" << std::endl;

                    for(uint j = 0; j < _vertexAmount; j++) 
                        _matrix[i][j] = INT_MAX;
                }
            }

            for(uint i = 0; i < _edgeAmount; i++) {
            
                file >> v0 >> v1 >> weight;
                if(file.fail()) {
                
                    std::cout << "Blad odczytu danych, udalo sie wczytac: " << i << " danych poprawnie, przerywam dalsze wczytywanie" << std::endl; 
                    break; 
                } 
                else {

                    _matrix[v0][v1] = weight;
                }
            }
        }

            file.close(); 
    }
    else 
        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 

}




/*
jezeli directed == true to alg wygeneruje graf skierowany, jezeli false to nieskierowany
jezeli connected == true to alg wygeneruje graf spojny, jezeli false to niespojny

zwraca 1 jesli wszystko dobrze
zwraca 0 jesli nie uda mu sie zaalokowac pamieci do reprezentacji grafu
zwraca -1 jesli podales niedodatnia liczbe wierzcholkow lub krawedzi
zwraca -2 jesli chcesz zrobic graf z wieksza liczba krawedzi niz w grafie o danej wielkosci jest to mozliwe
*/
int GraphMatrix::MakeRandomGraph(int vertexAmount, int edgeAmount, bool directed, bool connected, bool nonnegativeOnly) {

    if(edgeAmount <= 0 || vertexAmount <= 0) return -1;

    uint maxEdgeAmount = vertexAmount * (vertexAmount - 1) / 2;

    if((uint)edgeAmount > maxEdgeAmount) return -2;

    Delete();

    _edgeAmount = edgeAmount;
    _vertexAmount = vertexAmount;
    _firstVertex = 0;
    _lastVertex = 0;

    _matrix.reset(new std::unique_ptr<int[]>[_vertexAmount]);
    if(_matrix == nullptr) return 0;
    else {
        for(uint i = 0; i < _vertexAmount; i++) {

            _matrix[i].reset(new (std::nothrow) int[_vertexAmount]);
            if(_matrix[i] == nullptr) return 0;

            for(uint j = 0; j < _vertexAmount; j++) 
                _matrix[i][j] = INT_MAX;
        }
    }

    uint v0, v1;
    int weight;
    bool allVertexUsed = false;

    std::vector<uint> alreadyUsedVertex;

    std::mt19937 randomEngine(time(0));

    std::uniform_int_distribution<int> distributionForWeight(INT_MIN + 10, INT_MAX - 10);
    std::uniform_int_distribution<int> distributionForVertex(0, vertexAmount -1);

    auto weightGenerator = std::bind(distributionForWeight, randomEngine);
    auto vertexGenerator = std::bind(distributionForVertex, randomEngine);

    uint tempEdgeAmount = 0;
    while(tempEdgeAmount != _edgeAmount) {

        if(connected) {

            if(alreadyUsedVertex.size() == 0) {

                v0 = vertexGenerator();
                v1 = vertexGenerator();
            }
            else {

                if(!allVertexUsed) {

                    v0 = vertexGenerator();
                    v1 = alreadyUsedVertex[vertexGenerator() % alreadyUsedVertex.size()];
                    bool vertexAlreadyAdded = false;

                    for(uint i = 0; i < alreadyUsedVertex.size(); i++)
                        if(alreadyUsedVertex[i] == v0) {
                            vertexAlreadyAdded = true;
                            break;
                        }

                    if(vertexAlreadyAdded)
                            continue;       
                }
                else {

                    v0 = vertexGenerator();
                    v1 = vertexGenerator();

                }
            }


            if(v0 == v1) continue; //nie chcemy petli w grafie
            if(_matrix[v0][v1] != INT_MAX) continue; //taka krawedz juz istnieje

            if(nonnegativeOnly)
                weight = abs(weightGenerator());
            else
                weight = weightGenerator();

                
            _matrix[v0][v1] = weight;

            if(!directed)
                _matrix[v1][v0] = weight;

            tempEdgeAmount++;

            if(!allVertexUsed) {

                if(alreadyUsedVertex.size() == 0)
                    alreadyUsedVertex.push_back(v1); 

                alreadyUsedVertex.push_back(v0);
                if(alreadyUsedVertex.size() == _vertexAmount)
                    allVertexUsed = true;
            }

        }
        else {

            v0 = vertexGenerator();
            v1 = vertexGenerator();

            if(v0 == v1) continue; //nie chcemy petli w grafie
            if(_matrix[v0][v1] != INT_MAX) continue; //taka krawedz juz istnieje

            if(nonnegativeOnly)
                weight = abs(weightGenerator());
            else
                weight = weightGenerator();

            _matrix[v0][v1] = weight;

            if(!directed)
                _matrix[v1][v0] = weight;

            tempEdgeAmount++;

            
        }

    }

    _firstVertex = v0;

    return 1;

}


std::vector<DistanceWithPrevious> GraphMatrix::SPPDijsktra() {

    std::vector<DistanceWithPrevious> result(_vertexAmount);

    for(uint i=0; i < result.size(); ++i) {
        result[i].Distance = LONG_MAX;
        result[i].Previous = -1;
    }

    result[_firstVertex].Distance = 0;

    auto comp = [](const VertexWithDistance &a, const VertexWithDistance &b) { return a.Distance > b.Distance; };
    std::priority_queue<VertexWithDistance, std::vector<VertexWithDistance>, decltype(comp)> queue(comp);

    for(uint i = 0; i < result.size(); ++i) 
        queue.push(VertexWithDistance(i, result[i].Distance));

    while(!queue.empty()) {

        VertexWithDistance temp = queue.top();

        for(uint i = 0; i < _vertexAmount; ++i) 
            if(_matrix[temp.Vertex][i] != INT_MAX )  {
                long tempDistance = temp.Distance + _matrix[temp.Vertex][i];

                if(tempDistance < result[i].Distance && temp.Distance != LONG_MAX) {

                    result[i].Distance = tempDistance;
                    result[i].Previous = temp.Vertex;
                    queue.push(VertexWithDistance(i, tempDistance));

                }
        }

        queue.pop();
     
    }

    return result;
}

std::vector<DistanceWithPrevious> GraphMatrix::SPPDBellmanFord() {

    std::vector<DistanceWithPrevious> result(_vertexAmount);

    for(uint i=0; i < result.size(); ++i) {
        result[i].Distance = LONG_MAX;
        result[i].Previous = -1;
    }

    result[_firstVertex].Distance = 0;

    uint z = _vertexAmount - 1;
    while(z--) {

        for(uint i = 0; i < _vertexAmount; ++i)
            for(uint j = 0; j < _vertexAmount; ++j) 
                if(_matrix[i][j] != INT_MAX )  {

                    long tempDistance = result[i].Distance + _matrix[i][j];

                    if(tempDistance < result[j].Distance && result[i].Distance != LONG_MAX) {

                        result[j].Distance = tempDistance;
                        result[j].Previous = i;
                    }
                }
    }

    return result;

}

void GraphMatrix::printSPP(std::vector<DistanceWithPrevious> result) {

    bool startVertexFound = false;

    for(size_t i = 0; i < result.size(); ++i)
        if(result[i].Previous == -1)
            startVertexFound = true;

    if(startVertexFound) {

        for(size_t i = 0; i < result.size(); ++i) {
        
            std::cout << i << " - dist: ";
            if(result[i].Distance == LONG_MAX)
                std::cout << "inf";
            else
                std::cout << result[i].Distance;
                
            std::cout << " path: ";
            std::vector<int> path;

            path.push_back(i);
            int prev = result[i].Previous;

            while(prev != -1) {

                path.push_back(prev);
                prev = result[prev].Previous;
            }

            for(size_t i = path.size() - 1; i > 0; --i) {

                std::cout << path[i] << " ";
            }

            std::cout << std::endl;
        }
    }
    else
        std::cout << "Wykryto ujemny cykl!" << std::endl;

}
