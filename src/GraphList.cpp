#include"GraphList.h"

GraphList::GraphList() {
    
    _edgeAmount = 0;
    _vertexAmount = 0;
    _firstVertex = 0;
    _lastVertex = 0;

}

GraphList::~GraphList() {

    Delete();    
}


void GraphList::Delete() {

    _edgeAmount = 0;
    _vertexAmount = 0;
    _firstVertex = 0;
    _lastVertex = 0;

    for(uint i = 0; i < _vertexAmount; i++)
        _tab[i].clear();

    _tab.reset(nullptr);
            
}

void GraphList::SetVertex() {

    std::cout << "Prosze podac wierzcholek poczatkowy i koncowy: ";
    std::cin >>  _firstVertex >> _lastVertex;

}


void GraphList::Print() {

    for(uint i = 0; i < _vertexAmount; i++) {

        std::cout << i << ": ";
        for(std::list<VertexWithWeight>::iterator it = _tab[i].begin(); it != _tab[i].end(); ++it)
                std::cout << "(" << (*it).Vertex << " " << (*it).Weight << ") ";
       
        std::cout << std::endl;
    }

   // std::cout << "frstVertex: " << _firstVertex << std::endl;
    
}


void GraphList::PrintMST(std::list<Edge> finalEdgeList) {

    long totalWeight = 0;

    while(!finalEdgeList.empty()) {
        totalWeight += finalEdgeList.front().Weight;
        std::cout << finalEdgeList.front().FirstVertex << " " << finalEdgeList.front().SecondVertex << " " << finalEdgeList.front().Weight << std::endl;
        finalEdgeList.pop_front();

    }

    std::cout << "Waga MST: " << totalWeight << std::endl;

}



/*
Wersja wczytujaca graf skierowany
moze wczytac graf nieskierowany zakładając, że użytkownik pod dana krawedz dwukrotnie, raz w jedna strone, a raz w druga,
przy czym podanie raz jednej wagi, a raz innej spowoduje niezdefiniowane zachowanie algorytmu 


zwraca 1 jesli wszystko dobrze
zwraca 0 jesli nie uda mu sie zaalokowac pamieci do reprezentacji grafu
*/
int GraphList::ReadDirectedGraphToMemory() {

    Delete();

    uint v0, v1;
    int weight;
    std::cin >> _edgeAmount >> _vertexAmount >> _firstVertex >> _lastVertex;

    _tab.reset(new std::list<VertexWithWeight>[_vertexAmount]);
    if(_tab == nullptr) return 0;

    for(uint i = 0; i < _edgeAmount; i++) {

        std::cin >> v0 >> v1 >> weight;
        _tab[v0].push_back(VertexWithWeight(v1, weight));

    }

    return 1;

}


/*
Wersja wczytujaca graf nieskierowany, przy zalozeniu, ze uzytkownik podaje dana krawedz tylko raz w jedna ze stron
Algorytm sam dopisze sobie w pamieci, ze dana krawedz idzie takze w druga strone z taka sama waga (krawedz nieskierowana)

zwraca 1 jesli wszystko dobrze
zwraca 0 jesli nie uda mu sie zaalokowac pamieci do reprezentacji grafu
*/
int GraphList::ReadUndirectedGraphToMemory() {


    Delete();

    uint v0, v1;
    int weight;
    std::cin >> _edgeAmount >> _vertexAmount >> _firstVertex >> _lastVertex;

    _tab.reset(new std::list<VertexWithWeight>[_vertexAmount]);
    if(_tab == nullptr) return 0;

    for(uint i = 0; i < _edgeAmount; i++) {

        std::cin >> v0 >> v1 >> weight;
        _tab[v0].push_back(VertexWithWeight(v1, weight));
        _tab[v1].push_back(VertexWithWeight(v0, weight));

    }

    return 1;

}




std::list<Edge> GraphList::MSTPrima() {

//w pamieci zakladam, ze graf jest trzymany jako nieskierowany

    auto comp = [](const Edge &a, const Edge &b) { return a.Weight > b.Weight; };
    std::priority_queue<Edge, std::vector<Edge>, decltype(comp)> tempEdgeList(comp);
    std::list<Edge> finalEdgeList;
    

    bool usedVertex[_vertexAmount];

    for(uint i = 0; i < _vertexAmount; i++) 
        usedVertex[i] = false;


    uint lastAddedVertex = _firstVertex;

    usedVertex[lastAddedVertex] = true;

    while(!(finalEdgeList.size() == _vertexAmount -1)) {

        for(std::list<VertexWithWeight>::iterator it = _tab[lastAddedVertex].begin(); it != _tab[lastAddedVertex].end(); ++it)
            tempEdgeList.push(Edge(lastAddedVertex, (*it).Vertex, (*it).Weight));


        while(1) {

            if(usedVertex[tempEdgeList.top().FirstVertex] == true && usedVertex[tempEdgeList.top().SecondVertex] == true) {

                tempEdgeList.pop();
            }
            else {

                finalEdgeList.push_back(tempEdgeList.top());

                if(usedVertex[tempEdgeList.top().FirstVertex] == false) {

                    lastAddedVertex = tempEdgeList.top().FirstVertex;
                    usedVertex[tempEdgeList.top().FirstVertex] = true;
                }
                else {

                    lastAddedVertex = tempEdgeList.top().SecondVertex;
                    usedVertex[tempEdgeList.top().SecondVertex] = true;
                }

                tempEdgeList.pop();
                break;
            }
        }
    }
  

    return finalEdgeList;

}

 
std::list<Edge> GraphList::MSTKruskal() {

//w pamieci zakladam, ze graf jest trzymany jako nieskierowany

    std::list<Edge> initialEdgeList;
    std::list<Edge> finalEdgeList;


    for(uint i = 0; i < _vertexAmount; i++) 
        for(std::list<VertexWithWeight>::iterator it = _tab[i].begin(); it != _tab[i].end(); ++it)
            initialEdgeList.push_front(Edge(i, (*it).Vertex, (*it).Weight));

    initialEdgeList.sort([](const Edge &a, const Edge &b) { return a.Weight < b.Weight; }); 
        
    uint tab[_vertexAmount];

    for(uint i = 0; i < _vertexAmount; i++) 
        tab[i] = i;


    while(  !(finalEdgeList.size() == _vertexAmount -1)  && !initialEdgeList.empty()  ) {

        if(tab[initialEdgeList.front().FirstVertex] != tab[initialEdgeList.front().SecondVertex]) {

            uint tempID = tab[initialEdgeList.front().SecondVertex];
            for(uint i = 0; i < _vertexAmount; i++) 
                if(tab[i] == tempID)
                    tab[i] = tab[initialEdgeList.front().FirstVertex];


            finalEdgeList.push_back(initialEdgeList.front());
            initialEdgeList.pop_front();


        }
        else {

            initialEdgeList.pop_front();
        }
    }

    

    return finalEdgeList;

}



void GraphList::MakeUndirecteGraphFromFile(std::string fileName) {

    Delete();

    uint v0, v1;
    int weight;

    std::ifstream file;
    file.open(fileName.c_str());

    if(file.is_open()) {
    
        file >> _edgeAmount >> _vertexAmount >> _firstVertex >> _lastVertex;
        if(file.fail()) 
            std::cout << "Nie udalo sie wczytac pierwszej linii danych ogolnych z pliku" << std::endl; 
        else {
            
            
            _tab.reset(new std::list<VertexWithWeight>[_vertexAmount]);
             if(_tab == nullptr)
                std::cout << "Nie udalo zaalkowac pamieci dla grafu" << std::endl; 

            for(uint i = 0; i < _edgeAmount; i++) {
            
                file >> v0 >> v1 >> weight;
                if(file.fail()) {
                
                    std::cout << "Blad odczytu danych, udalo sie wczytac: " << i << " danych poprawnie, przerywam dalsze wczytywanie" << std::endl; 
                    break; 
                } 
                else {
                    
                    _tab[v0].push_back(VertexWithWeight(v1, weight));
                    _tab[v1].push_back(VertexWithWeight(v0, weight));
                }
            }
        }

            file.close(); 
    }
    else 
        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 

}







void GraphList::MakeDirecteGraphFromFile(std::string fileName) {

    Delete();

    uint v0, v1;
    int weight;

    std::ifstream file;
    file.open(fileName.c_str());

    if(file.is_open()) {
    
        file >> _edgeAmount >> _vertexAmount >> _firstVertex >> _lastVertex;
        if(file.fail()) 
            std::cout << "Nie udalo sie wczytac pierwszej linii danych ogolnych z pliku" << std::endl; 
        else {
            
            
            _tab.reset(new std::list<VertexWithWeight>[_vertexAmount]);
             if(_tab == nullptr)
                std::cout << "Nie udalo zaalkowac pamieci dla grafu" << std::endl; 

            for(uint i = 0; i < _edgeAmount; i++) {
            
                file >> v0 >> v1 >> weight;
                if(file.fail()) {
                
                    std::cout << "Blad odczytu danych, udalo sie wczytac: " << i << " danych poprawnie, przerywam dalsze wczytywanie" << std::endl; 
                    break; 
                } 
                else {

                    _tab[v0].push_back(VertexWithWeight(v1, weight));
                }
            }
        }

            file.close(); 
    }
    else 
        std::cout << "Nie udalo sie otworzyc pliku o podanej nazwie" << std::endl; 

}


/*
jezeli directed == true to alg wygeneruje graf skierowany, jezeli false to nieskierowany
jezeli connected == true to alg wygeneruje graf spojny, jezeli false to niespojny

zwraca 1 jesli wszystko dobrze
zwraca 0 jesli nie uda mu sie zaalokowac pamieci do reprezentacji grafu
zwraca -1 jesli podales niedodatnia liczbe wierzcholkow lub krawedzi
zwraca -2 jesli chcesz zrobic graf z wieksza liczba krawedzi niz w grafie o danej wielkosci jest to mozliwe
*/
int GraphList::MakeRandomGraph(int vertexAmount, int edgeAmount, bool directed, bool connected, bool nonnegativeOnly) {

    if(edgeAmount <= 0 || vertexAmount <= 0) return -1;

    uint maxEdgeAmount = vertexAmount * (vertexAmount - 1) / 2;

    if((uint)edgeAmount > maxEdgeAmount) return -2;

    Delete();

    _edgeAmount = edgeAmount;
    _vertexAmount = vertexAmount;
    _firstVertex = 0;
    _lastVertex = 0;

    _tab.reset(new std::list<VertexWithWeight>[_vertexAmount]);
    if(_tab == nullptr) return 0;


    uint v0, v1;
    int weight;
    bool allVertexUsed = false;

    std::vector<uint> alreadyUsedVertex;

    std::mt19937 randomEngine(time(0));

    std::uniform_int_distribution<int> distributionForWeight(INT_MIN + 10, INT_MAX - 10);
    std::uniform_int_distribution<int> distributionForVertex(0, vertexAmount -1);

    auto weightGenerator = std::bind(distributionForWeight, randomEngine);
    auto vertexGenerator = std::bind(distributionForVertex, randomEngine);

    uint tempEdgeAmount = 0;
    while(tempEdgeAmount != _edgeAmount) {

        if(connected) {

            if(alreadyUsedVertex.size() == 0) {

                v0 = vertexGenerator();
                v1 = vertexGenerator();
            }
            else {

                if(!allVertexUsed) {

                    v0 = vertexGenerator();
                    v1 = alreadyUsedVertex[vertexGenerator() % alreadyUsedVertex.size()];
                    bool vertexAlreadyAdded = false;

                    for(uint i = 0; i < alreadyUsedVertex.size(); i++)
                        if(alreadyUsedVertex[i] == v0) {
                            vertexAlreadyAdded = true;
                            break;
                        }

                    if(vertexAlreadyAdded)
                            continue;       
                }
                else {

                    v0 = vertexGenerator();
                    v1 = vertexGenerator();

                }
            }

            

            if(v0 == v1) continue; //nie chcemy petli w grafie (sam na siebie)

            bool edgeAlreadyExist = false;
            for(std::list<VertexWithWeight>::iterator it = _tab[v0].begin(); it != _tab[v0].end(); ++it)
                if((*it).Vertex == v1)
                    edgeAlreadyExist = true; //taka krawedz juz istnieje

            if(edgeAlreadyExist) continue;

            if(nonnegativeOnly)
                weight = abs(weightGenerator());
            else
                weight = weightGenerator();

            
            _tab[v0].push_back(VertexWithWeight(v1, weight));

            if(!directed)
                _tab[v1].push_back(VertexWithWeight(v0, weight));

            tempEdgeAmount++;

            if(!allVertexUsed) {

                if(alreadyUsedVertex.size() == 0)
                    alreadyUsedVertex.push_back(v1); 

                alreadyUsedVertex.push_back(v0);
                if(alreadyUsedVertex.size() == _vertexAmount)
                    allVertexUsed = true;
            }

        }
        else {

            v0 = vertexGenerator();
            v1 = vertexGenerator();

            if(v0 == v1) continue; //nie chcemy petli w grafie


            bool edgeAlreadyExist = false;
            for(std::list<VertexWithWeight>::iterator it = _tab[v0].begin(); it != _tab[v0].end(); ++it)
                if((*it).Vertex == v1)
                    edgeAlreadyExist = true; //taka krawedz juz istnieje

            if(edgeAlreadyExist) continue;

            if(nonnegativeOnly)
                weight = abs(weightGenerator());
            else
                weight = weightGenerator();

            _tab[v0].push_back(VertexWithWeight(v1, weight));

            if(!directed)
                _tab[v1].push_back(VertexWithWeight(v0, weight));

            tempEdgeAmount++;
        }

    }

    _firstVertex = v0;

    return 1;

}

std::vector<DistanceWithPrevious> GraphList::SPPDijsktra() {

    std::vector<DistanceWithPrevious> result(_vertexAmount);

    for(uint i=0; i < result.size(); ++i) {
        result[i].Distance = LONG_MAX;
        result[i].Previous = -1;
    }

    result[_firstVertex].Distance = 0;

    auto comp = [](const VertexWithDistance &a, const VertexWithDistance &b) { return a.Distance > b.Distance; };
    std::priority_queue<VertexWithDistance, std::vector<VertexWithDistance>, decltype(comp)> queue(comp);

    for(uint i = 0; i < result.size(); ++i) 
        queue.push(VertexWithDistance(i, result[i].Distance));

    while(!queue.empty()) {

        VertexWithDistance temp = queue.top();

        for(std::list<VertexWithWeight>::iterator it = _tab[temp.Vertex].begin(); it != _tab[temp.Vertex].end(); ++it) {
            long tempDistance = temp.Distance + (*it).Weight;

            if(tempDistance < result[(*it).Vertex].Distance && temp.Distance != LONG_MAX) {

                result[(*it).Vertex].Distance = tempDistance;
                result[(*it).Vertex].Previous = temp.Vertex;
                queue.push(VertexWithDistance((*it).Vertex, tempDistance));

            }
        }

        queue.pop();
     
    }

    return result;
}



std::vector<DistanceWithPrevious> GraphList::SPPDBellmanFord() {

    std::vector<DistanceWithPrevious> result(_vertexAmount);

    for(uint i=0; i < result.size(); ++i) {
        result[i].Distance = LONG_MAX;
        result[i].Previous = -1;
    }

    result[_firstVertex].Distance = 0;

    uint z = _vertexAmount - 1;
    while(z--) {

        for(uint i = 0; i < _vertexAmount; ++i)
            for(std::list<VertexWithWeight>::iterator it = _tab[i].begin(); it != _tab[i].end(); ++it) {
                long tempDistance = result[i].Distance + (*it).Weight;

                if(tempDistance < result[(*it).Vertex].Distance && result[i].Distance != LONG_MAX) {

                    result[(*it).Vertex].Distance = tempDistance;
                    result[(*it).Vertex].Previous = i;
                }
            }

    }

    return result;

}

void GraphList::printSPP(std::vector<DistanceWithPrevious> result) {

    bool startVertexFound = false;

    for(size_t i = 0; i < result.size(); ++i)
        if(result[i].Previous == -1)
            startVertexFound = true;

    if(startVertexFound) {

        for(size_t i = 0; i < result.size(); ++i) {
        
            std::cout << i << " - dist: ";
            if(result[i].Distance == LONG_MAX)
                std::cout << "inf";
            else
                std::cout << result[i].Distance;
                
            std::cout << " path: ";
            std::vector<int> path;

            path.push_back(i);
            int prev = result[i].Previous;

            while(prev != -1) {

                path.push_back(prev);
                prev = result[prev].Previous;
            }

            for(size_t i = path.size() - 1; i > 0; --i) {

                std::cout << path[i] << " ";
            }

            std::cout << std::endl;
        }
    }
    else
        std::cout << "Wykryto ujemny cykl!" << std::endl;

}
