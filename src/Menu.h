#include<iostream>
#include<string>
#include<algorithm>
#include"GraphMatrix.h"
#include"GraphList.h"



#ifndef Menu_h
#define Menu_h

class Menu {

    private:
        GraphMatrix _menuGraphMatrix;
        GraphList _menuGraphList;

        void _MenuLoop();

        void _PrintGraphListHelp();
        void _PrintGraphMatrixHelp();
        void _PrintHelp();
        void _PrintNoSuchAction();
        void _PrintStart();
        
        void _SubMenuGraphList();
        void _SubMenuGraphMatrix();

    public:
        void Start();
};

#endif