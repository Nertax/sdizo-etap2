#include<iostream>
#include<memory>
#include<list>
#include<climits>
#include<fstream>
#include<random>
#include<queue>
#include<vector>
#include"Graph.h"


#ifndef GraphMatrix_h
#define GraphMatrix_h


class GraphMatrix {

    private:
        uint _edgeAmount, _vertexAmount, _firstVertex, _lastVertex;
        std::unique_ptr<std::unique_ptr<int[]>[]> _matrix;

    public:
        GraphMatrix();
        ~GraphMatrix(); 

        int ReadDirectedGraphToMemory();
        int ReadUndirectedGraphToMemory();
        void Print();
        void Delete();
        std::list<Edge> MSTKruskal();
        std::list<Edge> MSTPrima();
        void PrintMST(std::list<Edge> finalEdgeList);
        void MakeUndirecteGraphFromFile(std::string fileName);
        void MakeDirecteGraphFromFile(std::string fileName);
        int MakeRandomGraph(int vertexAmount, int edgeAmount, bool directed, bool connected, bool nonnegativeOnly);
        std::vector<DistanceWithPrevious> SPPDijsktra();
        void printSPP(std::vector<DistanceWithPrevious> result);
        std::vector<DistanceWithPrevious> SPPDBellmanFord();
        void SetVertex();
};

#endif